/*******************************************************************************
 *                  Edit the contract address of the domain.                   *
 *******************************************************************************/

function editAddressModal(domainName) {
    var editAddrInstance = M.Modal.getInstance(document.getElementById("editAddrModal"));

    $("#editAddrModal .progress").addClass("noOpacity");
    $("#editAddrModal").attr("data-domain", domainName);

    editAddrInstance.open();
}

async function doEditAddress(domainName, newAddress) {
    try {
        await contract.methods.changeContractAddress(domainName, newAddress).send({
            from: contract.defaultAccount,
            value: 0.05 * 10 ** 18
        });
    } catch (e) {
        $(modal).find(".progress").addClass("noOpacity");
        showToast(e);
    }
}

/*******************************************************************************
 *                  Switch the auction status of the domain.                   *
 *******************************************************************************/

function switchAuctionModal(domainName, newStatus) {
    var switchModal = M.Modal.getInstance(document.getElementById("switchAuctionModal"));

    $("#switchAuctionModal").attr("data-domain-name", domainName)
                            .attr("data-domain-new-status", newStatus);
    $("#switchAuctionModal .progress").addClass("noOpacity");
    $("#switchAuctionModal h4").text(!newStatus ? "Deny auctions" : "Allow auctions");
    $("#switchAuctionModal .op").text(!newStatus ? "deny" : "allow");
    $("#switchAuctionModal .domain-name").text(domainName);
    $("#switchAuctionBtn").text(!newStatus ? "Deny" : "Allow");

    switchModal.open();
}

function doSwitchAuctionStatus(domainName, newStatus) {
    $("#switchAuctionModal").find(".progress").removeClass("noOpacity");

    contract.methods.changeAuctionStatus(domainName, newStatus).send({
        from: contract.defaultAccount,
        value: 0.001 * 10 ** 18
    });
}

/*******************************************************************************
 *                     Managament of the offers of a domain.                   *
 *******************************************************************************/

function showOfferModal(domainName, ethAmount) {
    var offerModal = M.Modal.getInstance(document.getElementById("offerModal"));
    $("#offerModal").attr("data-domain-name", domainName);
    $("#offerModal input").val(ethAmount).focus();

    offerModal.open();
    $("#offerModal input").eq(0).focus();
}

/*******************************************************************************
 *                     Managament of the renewal of a domain.                  *
 *******************************************************************************/

function renewModal(domainName) {
    var renewModal = M.Modal.getInstance(document.getElementById("renewModal"));
    $("#renewModal").attr("data-domain-name", domainName);
    $("#renewModal .domain-name").text(domainName);

    renewModal.open();
}

/*******************************************************************************
 *                     Populate page sections at the startup.                  *
 *******************************************************************************/

async function populateSwitchModal() {
    const ul = $("#accountModal ul");
    const accounts = await web3.eth.getAccounts();

    for (let i = 0; i < accounts.length; i++) {
        const account = accounts[i];

        let item = $(ul).find("li.hidden").eq(0).clone();
        $(item).find(".title").text(account);

        const accountBalance = await web3.eth.getBalance(accounts[i]);
        $(item).find("p").text(web3.utils.fromWei(accountBalance.toString(), "ether") + " ETH");

        $(item).removeClass("hidden");

        if (account === contract.defaultAccount) {
            $(item).find(".selected-account").removeClass("hidden");
            $(ul).prepend(item);
        } else {
            $(ul).append(item);
        }

        $(item).click(function() {
            const selected = $(this).find(".title").text();
            sessionStorage.setItem("account", selected);
            location.reload();
        });
    }
}

$(window).ready(async function() {
    $("#doEditAddrBtn").click(function() {
        var modal = $(this).closest(".modal");
        const domainName = $(modal).attr("data-domain");
        const newAddress = $(modal).find("input").val();

        try {
            web3.utils.toChecksumAddress(newAddress);
            $(modal).find(".progress").removeClass("noOpacity");
            doEditAddress(domainName, newAddress);
        } catch (e) {
            $(modal).find(".progress").addClass("noOpacity");
            showToast("The address entered is not valid.");
        }
    });

    $("#switchAuctionBtn").click(function() {
        const _domainName = $(this).closest(".modal").attr("data-domain-name");
        const _status = $(this).closest(".modal").attr("data-domain-new-status");
        doSwitchAuctionStatus(_domainName, _status === "true");
    });

    $("#doRenewBtn").click(async function() {
        var modal = $(this).closest(".modal");
        const domainName = $(modal).attr("data-domain-name");

        try {
            $(modal).find(".progress").removeClass("noOpacity");

            await contract.methods.renewDomain(domainName).send({
                from: contract.defaultAccount,
                value: 0.5 * 10 ** 18
            });

            $(modal).find(".progress").addClass("noOpacity");
        } catch (e) {
            showToast(e);
        }
    });

    $("#doLookupBtn").click(async function() {
        var modal = $(this).closest(".modal");
        const domainName = $(modal).find("input").val();

        try {
            $(modal).find(".progress").removeClass("noOpacity");

            const address = await contract.methods.getContractAddress(domainName).call();

            $("#resultContractAddress").html("<strong>Contract address: </strong>" + address);
        } catch (e) {
            showToast(e);
        }

        $(modal).find(".progress").addClass("noOpacity");
    });

    $("#doBuyBtn").click(async function() {
        var modal = $(this).closest(".modal");
        const domainName = $(modal).find("input").val();

        try {
            $(modal).find(".progress").removeClass("noOpacity");

            const availCode = parseInt(await contract.methods.isDomainAvailable(domainName).call());

            switch (availCode) {
                case 0: //  Free domain.
                    await contract.methods.buyDomain(domainName).send({
                        from: contract.defaultAccount,
                        value: 1 * 10 ** 18,
                        gas: 6721975,
                        gasPrice: 100
                    });

                    location.reload();
                    break;
                case 1: //  Not free, but auction is possible.
                    showOfferModal(domainName, 1);
                    break;
                case 2: //  Not free, no auction is possible.
                    showToast(domainName + " can't be bought.");
                    break;
                case 3: //  It's yours.
                    showToast("You already own this domain.");
                    break;
            }

            $(modal).find(".progress").addClass("noOpacity");
        } catch (e) {
            $(modal).find(".progress").addClass("noOpacity");
            showToast(e);
        }
    });

    $("#doOfferBtn").click(async function() {
        var modal = $(this).closest(".modal");
        const domainName = $(modal).attr("data-domain-name");
        const ethAmount = parseFloat($(modal).find("input").val());

        try {
            $(modal).find(".progress").removeClass("noOpacity");

            await contract.methods.sendOffer(domainName).send({
                from: contract.defaultAccount,
                value: ethAmount * 10 ** 18 + 1 * 10 ** 16,
                gasLimit: 3 * 10 ** 5
            });

            location.reload();
        } catch (e) {
            $(modal).find(".progress").addClass("noOpacity");
            showToast(e);
        }
    });

    await populateSwitchModal();
});