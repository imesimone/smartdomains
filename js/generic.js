$(window).ready(function() {
    M.AutoInit();
});

function filterOutZeroAddresses(addresses) {
    var result = [];
    for (let i = 0; i < addresses.length; i++)
        if (!web3.utils.toBN(addresses[i]).isZero())
            result.push(addresses[i]);

    return result;
}

function showToast(message) {
    if (message.message)
        message = message.message;

    let warn = message.includes("error");

    message = message.replace("Returned error: VM Exception while processing transaction: revert", "");
    message = message.trim();
    M.toast({
        "html": (warn ? "\t&#9888; " : "") + message
    });
}