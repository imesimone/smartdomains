contract.events.DomainAuctionSwitched(function(error, result) {
	if (error)
		console.log(error);

	const   domainName  = result.returnValues[0],
			newStatus   = result.returnValues[1];

	let item = $("#domainsList").find("[data-domain='" + domainName + "']");
	$(item).find(".domainInfo_auction").text(newStatus ? "Yes" : "No");
	$(item).find(".auction_btn span").text(newStatus ? "Deny auctions" : "Allow auctions");
	$(item).find(".auction_btn i").text(newStatus ? "block" : "done_all");
	$(item).find(".auction_btn").attr("data-new-status", !newStatus);

	M.Modal.getInstance(document.getElementById("switchAuctionModal")).close();
	showToast("Auctions have been " + (newStatus ? "allowed" : "denied"));
});

contract.events.DomainContractAddressChanged(function(error, result) {
	if (error)
		console.log(error);

	const   domainName  = result.returnValues[0],
			newAddress  = result.returnValues[1];

	let item = $("#domainsList").find("[data-domain='" + domainName + "']");
	$(item).find(".domainInfo_address").text(newAddress);

	M.Modal.getInstance(document.getElementById("editAddrModal")).close();
	showToast("The domains's contract address has been updated.");
});

contract.events.DomainRenewed(function(error, result) {
	if (error)
		console.log(error);

	const   domainName  = result.returnValues[0],
			newExpiry   = result.returnValues[1];

	let expiryDate = new Date(newExpiry * 1000);
	expiryDate.setHours(0,0,0,0);

	let item = $("#domainsList").find("[data-domain='" + domainName + "']");
	$(item).find(".domainInfo_expireDate").text(expiryDate.toLocaleDateString());
	$(item).find(".renew_btn, .expiring").remove();

	M.Modal.getInstance(document.getElementById("renewModal")).close();
	showToast("The domains's expiration date has been renewed.");
});