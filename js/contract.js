if (typeof web3 !== "undefined") {
    web3 = new Web3(web3.currentProvider);
} else {
    web3 = new Web3(new Web3.providers.WebsocketProvider("ws://localhost:7545"));
}

const contractAbi = [
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": false,
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            },
            {
                "indexed": false,
                "internalType": "bool",
                "name": "currentStatus",
                "type": "bool"
            }
        ],
        "name": "DomainAuctionSwitched",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": false,
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            },
            {
                "indexed": false,
                "internalType": "address",
                "name": "currentConractAddress",
                "type": "address"
            }
        ],
        "name": "DomainContractAddressChanged",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": false,
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            },
            {
                "indexed": false,
                "internalType": "uint256",
                "name": "expiryDate",
                "type": "uint256"
            }
        ],
        "name": "DomainRenewed",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": false,
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            },
            {
                "indexed": false,
                "internalType": "address",
                "name": "currentOwner",
                "type": "address"
            }
        ],
        "name": "DomainSold",
        "type": "event"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            },
            {
                "internalType": "address",
                "name": "sender",
                "type": "address"
            }
        ],
        "name": "acceptOffer",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            }
        ],
        "name": "buyDomain",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            }
        ],
        "name": "cancelOffer",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            },
            {
                "internalType": "bool",
                "name": "newAuctionStatus",
                "type": "bool"
            }
        ],
        "name": "changeAuctionStatus",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            },
            {
                "internalType": "address",
                "name": "newLocation",
                "type": "address"
            }
        ],
        "name": "changeContractAddress",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            },
            {
                "internalType": "address",
                "name": "owner",
                "type": "address"
            },
            {
                "internalType": "bool",
                "name": "auction",
                "type": "bool"
            },
            {
                "internalType": "uint256",
                "name": "expiry",
                "type": "uint256"
            }
        ],
        "name": "createDomain",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [],
        "name": "getAimedDomains",
        "outputs": [
            {
                "internalType": "string[]",
                "name": "",
                "type": "string[]"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            }
        ],
        "name": "getContractAddress",
        "outputs": [
            {
                "internalType": "address",
                "name": "",
                "type": "address"
            }
        ],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            }
        ],
        "name": "getDomainInfo",
        "outputs": [
            {
                "internalType": "address",
                "name": "contractAddress",
                "type": "address"
            },
            {
                "internalType": "bool",
                "name": "auctionPossible",
                "type": "bool"
            },
            {
                "internalType": "uint256",
                "name": "expiryDate",
                "type": "uint256"
            }
        ],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [],
        "name": "getDomainsList",
        "outputs": [
            {
                "internalType": "string[]",
                "name": "",
                "type": "string[]"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            },
            {
                "internalType": "address",
                "name": "offerer",
                "type": "address"
            }
        ],
        "name": "getOfferInfo",
        "outputs": [
            {
                "internalType": "uint256",
                "name": "amount",
                "type": "uint256"
            },
            {
                "internalType": "uint8",
                "name": "status",
                "type": "uint8"
            },
            {
                "internalType": "bool",
                "name": "valid",
                "type": "bool"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            }
        ],
        "name": "getOfferers",
        "outputs": [
            {
                "internalType": "address[]",
                "name": "",
                "type": "address[]"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            }
        ],
        "name": "getOwner",
        "outputs": [
            {
                "internalType": "address",
                "name": "",
                "type": "address"
            }
        ],
        "stateMutability": "payable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            }
        ],
        "name": "isDomainAvailable",
        "outputs": [
            {
                "internalType": "uint8",
                "name": "",
                "type": "uint8"
            }
        ],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            },
            {
                "internalType": "address payable",
                "name": "sender",
                "type": "address"
            }
        ],
        "name": "refuseOffer",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            }
        ],
        "name": "renewDomain",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "domainName",
                "type": "string"
            }
        ],
        "name": "sendOffer",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function"
    }
];

const contractAddress = "0x1dA9bF91fb71186c90851B310eA3eE2f843E6334";
const contract = new web3.eth.Contract(contractAbi, contractAddress);

const GET_ACCOUNT = sessionStorage.getItem("account");

web3.eth.getAccounts().then(function (accounts) {
    if (accounts.length === 0) {
        window.location.href = "no_accounts.html";
        return;
    }

    if (GET_ACCOUNT === null) {
        sessionStorage.setItem("account", accounts[0]);
        location.reload();
    }

    for (let i = 0; i < accounts.length; i++) {
        if (accounts[i] === GET_ACCOUNT) {
            contract.defaultAccount = GET_ACCOUNT;
            web3.eth.defaultAccount = GET_ACCOUNT;
            break;
        }
    }

    $(window).ready(function() {
        $("#accountAddress").text(contract.defaultAccount);
        populateDomains();
        populateOffersSent();
    });
});

async function appendOfferItem(domainItem, offerItem) {
    $(offerItem).find(".acceptBtn").click(async function () {
        const domainName = $(domainItem).find(".domainInfo_name").text();
        const offerItem = $(this).closest(".offerItem");
        const buyer = $(offerItem).find(".offerer").text();
        const buyerAddr = web3.utils.toChecksumAddress(buyer);

        console.log("Accepting offer by " + buyerAddr + " for " + domainName);

        try {
            let gas = await contract.methods.acceptOffer(domainName, buyerAddr).estimateGas({from: contract.defaultAccount});
            console.log("Gas needed: " + gas);

            await contract.methods.acceptOffer(domainName, buyerAddr).send({
                from: web3.eth.defaultAccount,
                gas: gas
            });

            location.reload();
        } catch (e) {
            console.log(e);
            showToast(e);
        }
    });

    $(offerItem).find(".refuseBtn").click(async function () {
        const domainName = $(domainItem).find(".domainInfo_name").text();

        const offerItem = $(this).closest(".offerItem");
        const buyer = $(offerItem).find(".offerer").text();
        const buyerAddr = web3.utils.toChecksumAddress(buyer);

        console.log("Refusing offer by " + buyerAddr + " for " + domainName);

        try {
            await contract.methods.refuseOffer(domainName, buyerAddr).send({
                from: web3.eth.defaultAccount
            });

            location.reload();
        } catch (e) {
            showToast(e);
        }
    });

    var offers = $(domainItem).find(".offersList");
    $(offers).append(offerItem);
}

async function createOfferItem(domainItem, offerer, ethAmount) {
    var offers = $(domainItem).find(".offersList");

    var offerItem = $(offers).find(".collection-item.hidden").clone();
    $(offerItem).find(".offerer").text(offerer);
    $(offerItem).find(".amount").text(ethAmount);

    $(offerItem).removeClass("hidden");
    await appendOfferItem(domainItem, offerItem);
}

async function retrieveOffers(domainItem) {
    const domainName = $(domainItem).attr("data-domain");

    let tempOfferers;
    try {
        tempOfferers = await contract.methods.getOfferers(domainName).call();
    } catch (e) {
        showToast(e);
    }

    const offerers = filterOutZeroAddresses(tempOfferers);

    for (let offererIndex = 0; offererIndex < offerers.length; offererIndex++) {
        const offerer = offerers[offererIndex];

        let offerInfo;
        try {
            offerInfo = await contract.methods.getOfferInfo(domainName, offerer).call();
        } catch (e) {
            showToast(e);
        }

        const amount = offerInfo[0];
        const strAmount = amount.toString();
        const ethAmount = web3.utils.fromWei(strAmount, "ether");
        const status = offerInfo[1];

        if (status > 0)
            continue;

        await createOfferItem(domainItem,offerer, ethAmount);
    }

    if ($(domainItem).find(".offersList .collection-item:not(.hidden)").length === 0)
        $(domainItem).find(".offersList").remove();
}

function isExpiring(expiryDate) {
    let nowPlusMonth = new Date();
    nowPlusMonth.setHours(0,0,0,0);
    nowPlusMonth.setMonth(new Date().getMonth() + 1);

    return nowPlusMonth.getTime() >= expiryDate.getTime();
}

async function appendDomainItem(domainName, contractAddress, auctionPossible, expiryDate) {
    let item = $(".domainItem").eq(0).clone();

    $(item).attr("data-domain", domainName);
    $(item).find(".domainInfo_name").text(domainName);
    $(item).find(".domainInfo_address").text(contractAddress);
    $(item).find(".domainInfo_expireDate").text(expiryDate.toLocaleDateString());
    $(item).find(".domainInfo_auction").text(auctionPossible ? "Yes" : "No");

    if (isExpiring(expiryDate))
        $(item).find(".collapsible-header .expiring, .renew_btn").removeClass("hidden");
    else
        $(item).find(".collapsible-header .offers").removeClass("second");

    $(item).find(".auction_btn").attr("data-new-status", !auctionPossible);
    $(item).find(".auction_btn i").text(auctionPossible ? "block" : "done_all");
    $(item).find(".auction_btn span").text(auctionPossible ? "Deny auctions" : "Allow auctions");

    $(item).find(".auction_btn").click(function() {
        const domainName = $(this).closest(".domainItem").attr("data-domain");
        const newStatus = $(this).attr("data-new-status") === "true";
        switchAuctionModal(domainName, newStatus);
    });

    $(item).find(".editAddrBtn").click(function() {
        const domainName = $(this).closest(".domainItem").attr("data-domain");
        editAddressModal(domainName);
    });

    $(item).find(".renew_btn").click(function() {
        const domainName = $(this).closest(".domainItem").attr("data-domain");
        renewModal(domainName);
    });

    await retrieveOffers(item);

    $(item).removeClass("hidden");
    $("#domainsList").append(item);
}

async function createDomainItem(domainName) {
    try {
        const domainInfo = await contract.methods.getDomainInfo(domainName).call();

        const contractAddress = domainInfo[0];
        const auctionPossible = domainInfo[1];
        const timestamp = domainInfo[2];

        let expiryDate = new Date(timestamp * 1000);
        expiryDate.setHours(0,0,0,0);

        await appendDomainItem(domainName, contractAddress, auctionPossible, expiryDate);
    } catch (e) {
        if (e.message.includes("404 - Domain not found."))
            return;
        showToast(e);
    }
}

async function populateDomains() {
    try {
        const domains = await contract.methods.getDomainsList().call();

        for (let d_i = 0; d_i < domains.length; d_i++) {
            const domainName = domains[d_i];
            await createDomainItem(domainName);
        }

        if ($("#domainsList .domainItem:not(.hidden)").length === 0) {
            $("#domainsList").remove();
            $("#emptyDomains").removeClass("hidden");
        }

    } catch (e) {
        showToast(e);
    }
}

async function createOfferSentItem(domainName, ethAmount, status) {
    var offersSentList = $("#offersSentList");

    var offerSentItem = $(offersSentList).find(".offerSentItem.hidden").clone();
    $(offerSentItem).attr("data-domain-name", domainName);
    $(offerSentItem).find(".title").text(domainName);
    $(offerSentItem).find(".amount").text(ethAmount);
    $(offerSentItem).attr("data-eth-amount", ethAmount.toString());
    $(offerSentItem).find(".chip-" + status.toString()).removeClass("hidden");

    if (status === 0) {
        $(offerSentItem).find(".withdrawBtn").click(async function() {
            const domainName = $(this).closest(".offerSentItem").attr("data-domain-name");

            try {
                await contract.methods.cancelOffer(domainName).send({
                    from: web3.eth.defaultAccount
                });
                location.reload();
            } catch (e) {
                showToast(e);
            }
        });
    } else {
        $(offerSentItem).find(".withdrawBtn").remove();
    }

    if (status === 2) {
        $(offerSentItem).find(".raiseBtn").click(async function() {
            const domainName = $(this).closest(".offerSentItem").attr("data-domain-name");
            const ethAmount = parseFloat($(this).closest(".offerSentItem").attr("data-eth-amount"));
            showOfferModal(domainName, ethAmount);
        });
    } else {
        $(offerSentItem).find(".raiseBtn").remove();
    }

    $(offerSentItem).removeClass("hidden");
    $(offersSentList).prepend(offerSentItem);
}

async function populateOffersSent() {
    const domains = await contract.methods.getAimedDomains().call();
    if (domains === null || domains === undefined)
        return;

    for (let i = 0; i < domains.length; i++) {
        const domainName = domains[i];

        let offerInfo;
        try {
            offerInfo = await contract.methods.getOfferInfo(domainName, web3.eth.defaultAccount).call();
        } catch (e) {
            showToast(e);
        }

        const amount = offerInfo[0];
        const strAmount = amount.toString();
        const ethAmount = web3.utils.fromWei(strAmount, "ether");
        const status = parseInt(offerInfo[1]);

        await createOfferSentItem(domainName, ethAmount, status);
    }
    if ($("#offersSentList .offerSentItem:not(.hidden)").length === 0) {
        $("#offersSentList").remove();
        $("#emptyOffers").removeClass("hidden");
    }
}