// SPDX-License-Identifier: UNLICENSED

pragma solidity >=0.4.0 <0.8.0;
pragma experimental ABIEncoderV2;

contract SmartDomains {

    /*
        Structs
    */

    enum OfferStatus {
        Pending,
        Withdrawed,
        Refused,
        Accepted
    }

    struct Offer {
        address sender;
        uint amount;
        OfferStatus status;
        bool valid;
    }

    struct Domain {
        string name;
        address contractAddress;
        address owner;
        bool auctionPossible;
        uint256 expiryDate;
    }

    /*
        Events
    */

    event DomainAuctionSwitched (
        string domainName,
        bool currentStatus
    );

    event DomainContractAddressChanged (
        string domainName,
        address currentConractAddress
    );

    event DomainSold (
        string domainName,
        address currentOwner
    );

    event DomainRenewed (
        string domainName,
        uint expiryDate
    );

    mapping(address => string[]) private owners;
    mapping(string => Domain) private domains;
    mapping(string => address[]) private offerers;
    mapping(string => mapping(address => Offer)) offers;
    mapping(address => string[]) userToDomainAimed;

    modifier DomainExists(string memory domainName) {
        require(isDomainAvailable(domainName) != 0, "404 - Domain not found.");
        _;
    }

    /*
        Basic functions to buy and manage a domain.
    */

    function buyDomain(string memory domainName) public payable {
        require(msg.value == 1 ether, "The creation of a domain costs 1 ETH.");
        require(validateDomain(domainName), "The domain name is not valid.");

        createDomain(domainName, msg.sender, false, block.timestamp + 365 days);

        emit DomainSold(domainName, msg.sender);
    }

    function createDomain(string memory domainName, address owner, bool auction, uint expiry) public {
        require(isDomainAvailable(domainName) == 0, "The domain is already owned by someone.");

        domains[domainName] = Domain({
            name: domainName,
            contractAddress: address(0),
            owner: owner,
            auctionPossible: auction,
            expiryDate: expiry
        });

        owners[owner].push(domainName);
    }

    function validateDomain(string memory domainName) pure private returns (bool) {
        require(bytes(domainName).length > 2, "The domain name has to be at least 2 chars.");
        return true;
    }

    function resetDomain(Domain memory domain) private {
        string memory domainName = domain.name;

        //  Remove the domains from the owner's list.
        address domainOwner = domain.owner;
        string[] memory ownerDomains = owners[domainOwner];

        for (uint256 i = 0; i < ownerDomains.length; i++) {
            string memory _domain = ownerDomains[i];
            if (keccak256(abi.encodePacked(_domain)) == keccak256(abi.encodePacked(domainName))) {
                delete owners[domainOwner][i];
                break;
            }
        }

        //  Remove the domain from the mapping.
        delete domains[domainName];
    }

    function forceResetDomain(Domain memory domain) private DomainExists(domain.name) {
        require(domain.owner == msg.sender, "Wait wait... You are not the owner of this domain!");

        resetDomain(domain);
    }

    function changeDomainOwner(string memory domainName, address newOwner) private {
        Domain memory oldDomain = domains[domainName];
        resetDomain(oldDomain);

        createDomain(domainName, newOwner, false, oldDomain.expiryDate);
    }

    function isDomainAvailable(string memory domainName) public returns (uint8) {
        Domain memory domain = domains[domainName];

        if (domain.expiryDate < block.timestamp){
            resetDomain(domain);
            domain = domains[domainName];
        }

        //  The domain is not owned by anyone.
        if (domain.owner == address(0))
            return 0;

        if (domain.owner == msg.sender)
            return 3;

        //  The domain is owned by someone. An offer can be sent.
        if (domain.auctionPossible)
            return 1;

        //  The domain is owned by someone. No offer can be sent.
        return 2;
    }

    function getDomainInfo(string memory domainName) public DomainExists(domainName) returns (address contractAddress, bool auctionPossible, uint256 expiryDate) {
        Domain memory domain = domains[domainName];
        return (domain.contractAddress, domain.auctionPossible, domain.expiryDate);
    }

    function getDomainsList() view public returns (string[] memory) {
        return owners[msg.sender];
    }

    function changeContractAddress(string memory domainName, address newLocation) public payable DomainExists(domainName) {
        require(msg.value == 0.05 ether, "Chaning the contract address of a domain costs 0.05 ETH.");

        Domain memory domain = domains[domainName];
        require(domain.owner == msg.sender, "Wait wait... You are not the owner of this domain!");

        domains[domainName].contractAddress = newLocation;

        emit DomainContractAddressChanged(domainName, newLocation);
    }

    function changeAuctionStatus(string memory domainName, bool newAuctionStatus) public payable DomainExists(domainName) {
        require(msg.value == 0.001 ether, "Chaning the auction status of a domain costs 0.001 ETH.");

        Domain memory domain = domains[domainName];
        require(domain.owner == msg.sender, "Wait wait... You are not the owner of this domain!");

        domains[domainName].auctionPossible = newAuctionStatus;

        emit DomainAuctionSwitched(domainName, newAuctionStatus);
    }

    function renewDomain(string memory domainName) public payable DomainExists(domainName) {
        Domain memory domain = domains[domainName];
        require(domain.owner == msg.sender, "Wait wait... You are not the owner of this domain!");
        require(msg.value == 0.5 ether, "The renewal of a domain costs 0.5 ETH.");

        domains[domainName].expiryDate += 365 days;

        emit DomainRenewed(domainName, domains[domainName].expiryDate);
    }

    /*
        Whois e Lookup
    */

    function getOwner(string memory domainName) public payable DomainExists(domainName) returns (address) {
        require(msg.value == 0.001 ether, "Whois information costs 0.001 ETH.");

        Domain memory domain = domains[domainName];
        return domain.owner;
    }

    function getContractAddress(string memory domainName) public DomainExists(domainName) returns (address) {
        Domain memory domain = domains[domainName];
        return domain.contractAddress;
    }

    /*
        Offers section.
    */

    function sendOffer(string memory domainName) public payable {
        uint8 availability = isDomainAvailable(domainName);
        require(availability != 0, "404 - Domain not found.");
        require(availability == 1, "The owner of the domain does not sell it.");

        Domain memory domain = domains[domainName];
        require(domain.owner != msg.sender, "You can't try to buy your own domain.");

        require(msg.value >= 0.01 ether, "The cost of an offer is at least 0.01 ETH.");
        uint offerAmount = msg.value - 0.01 ether;

        if (offers[domainName][msg.sender].status == OfferStatus.Pending &&
            offers[domainName][msg.sender].valid) {
            cancelOffer(domainName, msg.sender, true);
        }

        if (offers[domainName][msg.sender].sender == address(0)) {
            offerers[domainName].push(msg.sender);
            userToDomainAimed[msg.sender].push(domainName);
        }

        Offer memory o = Offer({
            sender: msg.sender,
            amount: offerAmount,
            status: OfferStatus(0),
            valid: true
        });

        offers[domainName][msg.sender] = o;
    }

    function getOfferers(string memory domainName) view public returns (address[] memory) {
        Domain memory domain = domains[domainName];
        require(domain.owner == msg.sender, "You are not the owner of the domain.");

        return offerers[domainName];
    }

    function getOfferInfo(string memory domainName, address offerer) view public returns (uint amount, uint8 status, bool valid) {
        Domain memory domain = domains[domainName];
        Offer memory o = offers[domainName][offerer];

        require(domain.owner == msg.sender || offerer == msg.sender, "You are not the owner of the domain.");

        return (o.amount, uint8(o.status), o.valid);
    }

    function getAimedDomains() view public returns (string[] memory) {
        return userToDomainAimed[msg.sender];
    }

    function cancelOffer(string memory domainName, address payable sender, bool canceled) private DomainExists(domainName) {

        require(offers[domainName][sender].status == OfferStatus.Pending, "The offer can't be canceled.");
        require(offers[domainName][sender].valid, "The offer is no more valid.");

        offers[domainName][sender].status = canceled ? OfferStatus.Withdrawed : OfferStatus.Refused;
        offers[domainName][sender].valid = false;

        sender.transfer(offers[domainName][sender].amount);
    }

    function cancelOffer(string memory domainName) public {
        cancelOffer(domainName, msg.sender, true);
    }

    function refuseOffer(string memory domainName, address payable sender) public {
        Domain memory domain = domains[domainName];
        require(domain.owner == msg.sender, "Wait wait... You are not the owner of this domain!");

        cancelOffer(domainName, sender, false);
    }

    function acceptOffer(string memory domainName, address sender) public {
        Domain memory domain = domains[domainName];
        require(domain.owner == msg.sender, "You are not the owner of the domain.");
        require(offers[domainName][sender].status == OfferStatus.Pending, "The offer can't be accepted.");
        require(offers[domainName][sender].valid, "The offer is no more valid.");

        offers[domainName][sender].status = OfferStatus.Accepted;

        address[] memory offerers = offerers[domainName];
        for (uint i = 0; i < offerers.length; i++) {
            address offerer = offerers[i];

            if (offerer != sender)
                refuseOffer(domainName, payable(offerer));
        }


        address payable seller = msg.sender;
        uint amount = offers[domainName][sender].amount;
        seller.transfer(amount);
        changeDomainOwner(domainName, sender);
    }
}